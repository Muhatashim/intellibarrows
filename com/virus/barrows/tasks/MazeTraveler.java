package com.virus.barrows.tasks;

import com.virus.barrows.api.utils.ConditionalTask;
import org.powerbot.script.methods.MethodContext;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/12/13
 * Time: 12:55 PM
 */
public class MazeTraveler extends ConditionalTask {

    private final MethodContext ctx;

    public MazeTraveler(MethodContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean activate() {
        return ctx.players.getLocal().getLocation().getPlane() == 3;
    }

    @Override
    protected int work() {
        return -1;
    }
}
