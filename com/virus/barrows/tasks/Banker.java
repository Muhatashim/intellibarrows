package com.virus.barrows.tasks;

import com.virus.barrows.Config;
import com.virus.barrows.api.combat.MyPlayer;
import com.virus.barrows.api.utils.ConditionalTask;
import org.powerbot.script.methods.Bank;
import org.powerbot.script.methods.MethodContext;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/13/13
 * Time: 5:22 PM
 */
public class Banker extends ConditionalTask {

    private final MethodContext ctx;

    public Banker(MethodContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean activate() {
        return MyPlayer.foodAmt() == 0 && ctx.players.getLocal().getHealthPercent() <= 70
                || ctx.bank.select().
                id(Bank.BANK_BOOTH_IDS, Bank.BANK_CHEST_IDS, Bank.BANK_COUNTER_IDS, Bank.BANK_NPC_IDS).size() > 0;
    }

    @Override
    protected int work() {
        if (MyPlayer.foodAmt() > 0)
            Config.get().getTransportationMethod().travel();
        else {
            ctx.bank.open();
            //TODO
            System.out.println("TODO");
        }

        return -1;
    }
}
