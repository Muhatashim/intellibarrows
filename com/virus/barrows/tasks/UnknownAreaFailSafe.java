package com.virus.barrows.tasks;

import com.virus.barrows.api.transportations.Transportation;
import com.virus.barrows.api.utils.ConditionalTask;
import org.powerbot.script.util.Timer;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/15/13
 * Time: 9:20 AM
 */
public class UnknownAreaFailSafe extends ConditionalTask {
    public static final Timer TIME_IN = new Timer(5000);

    @Override
    public boolean activate() {
        System.out.println(TIME_IN.toRemainingString());
        return !TIME_IN.isRunning();
    }

    @Override
    protected int work() {
        Transportation[] values = Transportation.values();

        for (Transportation value : values)   {
            if (value.travel())
                return -1;
        }

        return -1;
    }
}
