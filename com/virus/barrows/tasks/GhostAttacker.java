package com.virus.barrows.tasks;

import com.virus.barrows.Config;
import com.virus.barrows.api.Ghost;
import com.virus.barrows.api.combat.MyPlayer;
import com.virus.barrows.api.utils.ConditionalTask;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.Actor;
import org.powerbot.script.wrappers.Npc;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/12/13
 * Time: 10:05 AM
 */
public class GhostAttacker extends ConditionalTask {

    private final MethodContext ctx;
    private Ghost ghost;

    public GhostAttacker(MethodContext ctx) {
        this.ctx = ctx;
    }

    public void setGhost(Ghost ghost) {
        this.ghost = ghost;
    }

    public Ghost getGhost() {
        return ghost;
    }

    @Override
    public boolean activate() {
        return inCombat() && ghost != null && !ghost.isDead();
    }

    @Override
    protected int work() {
        if (MyPlayer.hasLowHealth() && !MyPlayer.eat()) {
            Config.get().getEmergencyTransport().travel();
            return -1;
        }

        if (Ghost.AKRISAE != ghost)
            ghost.getEnemyAttackStyle().defend();

        Actor interacting = ctx.players.getLocal().getInteracting();
        Npc interactingNpc = null;
        if (interacting instanceof Npc)
            interactingNpc = (Npc) interacting;

        Npc npc = toNpc();
        if (interactingNpc == null || !ghost.isMe(interactingNpc.getId())) {
            npc.click(true);
            if (!Sleep.waitForClick())
                return 0;
        }

        String s = "12345678ASDF";
        ctx.keyboard.send(s, false);

        return -1;
    }

    private Npc toNpc() {
        for (Npc npc : MyPlayer.getInteracters())
            if (npc != null && ghost.isMe(npc.getId()))
                return npc;

        return null;
    }

    public static Ghost getNext(Ghost except) {
        for (Ghost ghost : Ghost.values())
            if (except != ghost && !ghost.isDead() && ghost.use())
                return ghost;

        return null;
    }

    public boolean inCombat() {
        return toNpc() != null;
    }
}
