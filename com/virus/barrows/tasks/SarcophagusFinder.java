package com.virus.barrows.tasks;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.Area;
import com.virus.barrows.api.Ghost;
import com.virus.barrows.api.Sarcophagus;
import com.virus.barrows.api.chat.ChatOption;
import com.virus.barrows.api.chat.ChatOptions;
import com.virus.barrows.api.utils.ConditionalTask;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/12/13
 * Time: 9:59 AM
 */
public class SarcophagusFinder extends ConditionalTask {
    private final MethodContext ctx;
    private       Sarcophagus   fagWithTunnel;

    public SarcophagusFinder(MethodContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean activate() {
        return !Barrows.get().ghostAttacker.inCombat()
                && (Area.BARROWS.insideArea() || Sarcophagus.isInsideAMound());
    }

    @Override
    protected int work() {
        Ghost next = GhostAttacker.getNext(fagWithTunnel != null ? fagWithTunnel.getForGhost() : null);
        System.out.println(next);
        Sarcophagus sarcophagus;
        boolean finished = false;

        if (next == null) {
            if (GhostAttacker.getNext(null) == null) {
                System.out.println("Tele out");
                //TODO: Tele out
                return -1;
            } else if (fagWithTunnel != null) {
                sarcophagus = fagWithTunnel;
                next = fagWithTunnel.getForGhost();
                finished = true;
            } else {
                System.out.println("Bad logic");
                return -1;
            }
        } else
            sarcophagus = next.getSarcophagus();

        if (sarcophagus == null)
            return -1;

        if (!sarcophagus.insideMound()) {
            if (ctx.game.getPlane() != 0)
                Sarcophagus.gtfo();
            else {
                Tile moundLoc = sarcophagus.getMoundLoc();

                if (moundLoc.getMatrix(ctx).isOnScreen())
                    sarcophagus.digIn();
                else
                    ctx.movement.stepTowards(moundLoc);
            }

            Sleep.waitForMovement();
            return 0;
        } else {
            Barrows.get().ghostAttacker.setGhost(next);
            System.out.println("AAAAAAAAA " + finished);

            if (finished) {
                if (ChatOptions.getOptions().size() == 2) {
                    ctx.keyboard.send("1", true);
                    Sleep.waitForMovement();
                }
            }

            if (sarcophagus.search() == 2) {
                if (!finished) {
                    fagWithTunnel = sarcophagus;
                    return 0;
                } else {
                    ChatOption continueOption = ChatOptions.getContinueOption();

                    if (continueOption.getComponent().isValid())
                        ctx.keyboard.send(" ", true);
                }
            }
        }


        return -1;
    }
}
