package com.virus.barrows.tasks;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.Ghost;
import com.virus.barrows.api.combat.EquipmentSetup;
import com.virus.barrows.api.utils.ConditionalTask;
import org.powerbot.script.methods.MethodContext;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/22/13
 * Time: 2:43 PM
 */
public class EquipmentSwitcher extends ConditionalTask {

    private final MethodContext ctx;

    public EquipmentSwitcher(MethodContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public boolean activate() {
        Ghost currGhost = Barrows.get().ghostAttacker.getGhost();
        if (currGhost == null)
            return false;

        EquipmentSetup.Setup closest = EquipmentSetup.getClosestMatchToPlayer(ctx);
        return currGhost.getSetupType() != closest;
    }

    @Override
    protected int work() {
        Barrows.get().ghostAttacker.getGhost().getSetupType().getSetup().setup(ctx);
        return -1;
    }
}
