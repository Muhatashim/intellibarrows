package com.virus.barrows.ui;

import com.virus.barrows.Config;
import com.virus.barrows.ui.tabs.Info;
import com.virus.barrows.ui.tabs.Setups;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/24/13
 * Time: 3:51 PM
 */
public class MainUI extends Application {

    public static void main(String[] args) {
        Application.launch(MainUI.class);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        BorderPane borderPane = new BorderPane() {{
            setPadding(new Insets(10, 10, 10, 10));

            setCenter(new TabPane() {{
                getTabs().addAll(new Info(), new Setups());
                setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
            }});

            setBottom(new HBox() {{
                getChildren().add(
                        new Button("Start Script") {{
                            setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent actionEvent) {
                                    HBox parent = (HBox) getParent();
                                    parent.getChildren().remove(0);

                                    Config.get().setSetupCompleted();
                                }
                            });
                        }});

                setAlignment(Pos.CENTER);
            }});
        }};

        borderPane.setPrefSize(500, 500);
        stage.setScene(new Scene(borderPane));
        stage.show();

        stage.setResizable(false);
    }
}
