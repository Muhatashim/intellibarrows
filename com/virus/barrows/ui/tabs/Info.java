package com.virus.barrows.ui.tabs;

import com.virus.barrows.Barrows;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import org.powerbot.script.Manifest;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/24/13
 * Time: 5:06 PM
 */
public class Info extends Tab {

    public Info() {
        super("Info");

        FlowPane pane = new FlowPane(Orientation.VERTICAL);
        setContent(pane);

        pane.setVgap(15);

        final Manifest manifest = Barrows.get().getManifest();

        VBox dynSigBox = generateInfo(
                new Label("Dynamic sig link"),
                new TextField("No server yet") {{
                    setEditable(false);
                }});

        VBox threadBox = generateInfo(
                new Label("Thread link"),
                new TextField(manifest.website()) {{
                    setEditable(false);
                }});

        VBox versionBox = generateInfo(
                new Label("Current version"),
                new Text("[" + (manifest.hidden() ? "PRIVATE" : "PUBLIC") + " " + (manifest.vip() ? "VIP" : "FREE") + "] "
                        + String.valueOf(manifest.version())));

        VBox authorsBox = generateInfo(
                new Label("Authors"),
                new Text(Arrays.toString(manifest.authors())));

        VBox newsBox = generateInfo(
                new Label("News"),
                new TextArea("No server yet"){{
                    setEditable(false);
                }});

        pane.getChildren().addAll(dynSigBox, threadBox, versionBox, newsBox, authorsBox);
        pane.setAlignment(Pos.CENTER);
    }

    private VBox generateInfo(final Label label, final Node... forNodes) {
        return new VBox(5) {{
            getChildren().add(label);
            getChildren().addAll(forNodes);
        }};
    }
}
