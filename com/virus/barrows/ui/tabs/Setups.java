package com.virus.barrows.ui.tabs;

import com.virus.barrows.Barrows;
import com.virus.barrows.Config;
import com.virus.barrows.api.WearableItem;
import com.virus.barrows.api.combat.EquipmentSetup;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.powerbot.script.lang.ItemQuery;
import org.powerbot.script.methods.Equipment;
import org.powerbot.script.methods.Inventory;
import org.powerbot.script.util.Filter;
import org.powerbot.script.wrappers.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/24/13
 * Time: 5:58 PM
 */
public class Setups extends Tab {

    public Setups() {
        super("Setups");

        TabPane tabPane = new TabPane();
        setContent(tabPane);

        for (final EquipmentSetup.Setup setup : EquipmentSetup.Setup.values()) {
            tabPane.getTabs().add(new Tab(setup.name()) {{
                final List<Runnable> imageUpdaterTasks = new ArrayList<>(Equipment.Slot.values().length);
                BorderPane pane = new BorderPane();
                setContent(pane);

                final GridPane gridPane = makePaneFrom(setup, imageUpdaterTasks);
                gridPane.setAlignment(Pos.CENTER);
                pane.setCenter(gridPane);

                final Button updateButton = new Button("Set setup with currently wearing items");
                updateButton.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        for (Node node : gridPane.getChildren()) {
                            ImageView imgV = (ImageView) node;
                            imgV.setImage(null);
                        }

                        setup.setSetup(EquipmentSetup.newSetup(Barrows.get().getContext()));

                        for (Runnable runnable : imageUpdaterTasks)
                            Barrows.get().submit(runnable);
                    }
                });
                pane.setBottom(new VBox() {{
                    getChildren().add(updateButton);
                    setAlignment(Pos.CENTER);
                    setPadding(new Insets(20, 20, 20, 20));
                }});

                if (setup == EquipmentSetup.Setup.MAGE) {
                    FlowPane flowPane = makeRunesPane(imageUpdaterTasks);
                    pane.setTop(flowPane);
                }
            }});

            tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        }
    }

    private static String idToImageURL(int id) {
        return "http://www.runelocus.com/img/items/" + id + ".png";
    }

    private FlowPane makeRunesPane(List<Runnable> imageUpdaterTasks) {
        final FlowPane pane = new FlowPane(5, 5);
        final Inventory inventory = Barrows.get().getContext().inventory;

        for (int i = 0; i < 13; i++) {
            ImageView imageView = new ImageView();
            imageView.setEffect(new DropShadow() {{
                setInput(new Reflection());
            }});

            pane.getChildren().add(imageView);
        }

        imageUpdaterTasks.add(new Runnable() {
            @Override
            public void run() {
                ItemQuery<Item> runes = inventory.select().select(new Filter<Item>() {
                    @Override
                    public boolean accept(Item item) {
                        return item.getName().endsWith("rune");
                    }
                });

                for (Node node : pane.getChildren())
                    ((ImageView) node).setImage(null);

                int[] ids = new int[runes.size()];
                int i = 0;
                for (final Item item : runes) {
                    final Image image = new Image(idToImageURL(item.getId()));

                    ((ImageView) pane.getChildren().get(i))
                            .setImage(image);

                    ids[i++] = item.getId();
                }

                Config.get().setRunesIds(ids);
            }
        });

        return pane;
    }

    private GridPane makePaneFrom(final EquipmentSetup.Setup setup, List<Runnable> imageUpdaterTasks) {
        final GridPane gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(20);

        for (final Equipment.Slot value : Equipment.Slot.values()) {
            final ImageView imageView = new ImageView();
            imageView.setEffect(new DropShadow() {{
                setInput(new Reflection());
            }});

            imageUpdaterTasks.add(new Runnable() {
                @Override
                public void run() {
                    for (final WearableItem item : setup.getSetup().getItems())
                        if (item.getSlot() == value) {
                            Image image = new Image(idToImageURL(item.getId()));
                            imageView.setImage(image);
                        }
                }
            });

            int[] location = getLocation(value);
            gridPane.add(imageView, location[0], location[1]);
        }

        return gridPane;
    }

    private int[] getLocation(Equipment.Slot slot) {
        switch (slot) {
            default:
                return null;
            case AURA:
                return new int[]{0, 0};
            case HEAD:
                return new int[]{1, 0};
            case POCKET:
                return new int[]{2, 0};
            case CAPE:
                return new int[]{0, 1};
            case NECK:
                return new int[]{1, 1};
            case QUIVER:
                return new int[]{2, 1};
            case MAIN_HAND:
                return new int[]{0, 2};
            case TORSO:
                return new int[]{1, 2};
            case OFF_HAND:
                return new int[]{2, 2};
            case LEGS:
                return new int[]{1, 3};
            case HANDS:
                return new int[]{0, 4};
            case FEET:
                return new int[]{1, 4};
            case RING:
                return new int[]{2, 4};
        }
    }
}
