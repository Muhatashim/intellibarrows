package com.virus.barrows;

import com.virus.barrows.api.combat.EquipmentSetup;
import com.virus.barrows.api.combat.Food;
import com.virus.barrows.api.transportations.Transportation;
import com.virus.barrows.api.utils.Utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/13/13
 * Time: 11:00 AM
 */
public class Config {
    private static volatile Config           instance;
    private                 Food[]           food;
    private                 EquipmentSetup[] setups;
    private                 int[]            runesIds;
    private                 int              drinkBelow;
    private                 int              minHP;//% health to eat at.
    private                 int              randomHP;
    private                 boolean          randomizeMinHp;
    private                 Transportation   eTransport;
    private                 Transportation   transportationMethod;
    private                 boolean          setup;

    private Config() {
        food = new Food[0];
        minHP = 50;
        randomHP = minHP;
        randomizeMinHp = true;

        setups = new EquipmentSetup[3];
        runesIds = new int[0];

        eTransport = Transportation.VARROCK_TAB;
        transportationMethod = Transportation.DRAKANS_MADALLION;
    }

    public static synchronized Config get() {
        if (instance == null)
            return instance = new Config();

        return instance;
    }

    public int getDrinkBelow() {
        return drinkBelow;
    }

    public void setDrinkBelow(int drinkBelow) {
        this.drinkBelow = drinkBelow;
    }

    public boolean useRandomizeMinHp() {
        return randomizeMinHp;
    }

    public void setRandomizeMinHp(boolean randomizeMinHp) {
        this.randomizeMinHp = randomizeMinHp;
    }

    public int nextRandomHP() {
        return randomHP = Math.max(Utils.random(minHP - 15, minHP + 15), 10);
    }

    public int getRandomHP() {
        return randomHP;
    }

    public void setMinHP(int minHP) {
        this.minHP = minHP;
        nextRandomHP();
    }

    public int getMinHP() {
        return minHP;
    }

    public Food[] getFood() {
        return food;
    }

    public void setFood(Food[] food) {
        this.food = food;
    }

    public EquipmentSetup[] getSetups() {
        return setups;
    }

    public void setSetups(EquipmentSetup[] setups) {
        this.setups = setups;
    }

    public Transportation getEmergencyTransport() {
        return eTransport;
    }

    public void setEmergencyTransport(Transportation eTransport) {
        this.eTransport = eTransport;
    }

    public Transportation getTransportationMethod() {
        return transportationMethod;
    }

    public void setTransportationMethod(Transportation transportationMethod) {
        this.transportationMethod = transportationMethod;
    }

    public int[] getRunesIds() {
        return runesIds;
    }

    public void setRunesIds(int[] runesId) {
        this.runesIds = runesId;
    }

    public void setSetupCompleted() {
        this.setup = true;
    }

    public boolean isSetup() {
        return setup;
    }
}
