package com.virus.barrows.api.maze;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/16/13
 * Time: 1:18 PM
 */
public class Node {

    public        Node parent;
    public        Node child;
    private final Door me;

    public Node(Node parent, Node child, Door me) {
        this.parent = parent;
        this.child = child;
        this.me = me;
    }

    public Node getParent() {
        return parent;
    }

    public Node getChild() {
        return child;
    }

    public Door getMe() {
        return me;
    }

    public Node setParent(Node parent) {
        return this.parent = parent;
    }

    public Node setChild(Node child) {
        return this.child = child;
    }
}
