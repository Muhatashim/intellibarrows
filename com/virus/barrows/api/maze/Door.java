package com.virus.barrows.api.maze;

import com.virus.barrows.Barrows;
import org.powerbot.script.lang.ObjectQuery;
import org.powerbot.script.util.Timer;
import org.powerbot.script.wrappers.GameObject;
import org.powerbot.script.wrappers.Tile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/16/13
 * Time: 11:37 AM
 */
public class Door {

    public static final int[]  DOOR_IDS          = {
            6713, 6716, 6717, 6718, 6719, 6720, 6721, 6722, 6723,
            6724, 6725, 6726, 6727, 6728, 6729, 6730, 6731, 6732,
            6735, 6736, 6737, 6738, 6739, 6740, 6741, 6742, 6743,
            6744, 6745, 6746, 6747, 6748, 6749, 6750
    };
    public static final byte[] DOOR_DIST_DIFFERS = {
            4, 0,
            33, 0,
            6, 1,
            13, 0};

    private final Tile       at;
    private final Timer      forgetTimer;
    private       Boolean    canOpen;
    private       Door[]     connections;
    private       GameObject door;


    public Door(GameObject door) {
        this(door.getLocation());
        this.door = door;
    }

    public Door(Tile at) {
        this.at = at;
        forgetTimer = new Timer(1000 * 60 * 2);
    }

    public Tile getLocation() {
        return at;
    }

    public GameObject getDoor() {
        if (door == null || !door.isValid()) {
            Iterator<GameObject> iterator = Barrows.get().getContext().objects.select().at(at).first().iterator();
            if (iterator.hasNext())
                return iterator.next();
            return null;
        } else
            return door;
    }

    public static boolean isDoor(int id) {
        return Arrays.binarySearch(DOOR_IDS, id) >= 0;
    }

    public boolean canOpen() {
        return false;//TODO
    }

    public Door[] getConnections() {
        return connections == null || connections.length == 0 ? connections = findDoors() : connections;
    }

    private Door[] findDoors() {
        List<Door> doors = new ArrayList<>();

        for (int i = 0; i < DOOR_DIST_DIFFERS.length; i += 2) {
            byte differ = DOOR_DIST_DIFFERS[i];
            boolean diagnal = DOOR_DIST_DIFFERS[i + 1] != 0;

            for (int x = -1; x <= 1; x++)
                for (int y = -1; y <= 1; y++) {
                    if (diagnal && (x == 0 || y == 0)
                            || !diagnal && (x != 0 && y != 0))
                        continue;

                    Tile derive = at.derive(differ * x, differ * y);
                    ObjectQuery<GameObject> doorsAt = Barrows.get().getContext().objects.select().at(derive);

                    for (GameObject door : doorsAt)
                        if (isDoor(door.getId()))
                            doors.add(new Door(derive));
                }
        }

        return doors.toArray(new Door[doors.size()]);
    }

    @Override
    public String toString() {
        return at.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Door door1 = (Door) o;

        if (at != null ? !at.equals(door1.at) : door1.at != null) return false;
        if (door != null ? !door.equals(door1.door) : door1.door != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = at != null ? at.hashCode() : 0;
        result = 31 * result + (door != null ? door.hashCode() : 0);
        return result;
    }
}
