package com.virus.barrows.api.maze;

import com.virus.barrows.Barrows;
import org.powerbot.script.lang.Locatable;
import org.powerbot.script.lang.ObjectQuery;
import org.powerbot.script.util.Timer;
import org.powerbot.script.wrappers.GameObject;
import org.powerbot.script.wrappers.Tile;

import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/16/13
 * Time: 12:59 PM
 */
public class Maze {
    public static final int CHEST_ID = 10284;

    private Door[] doors;

    public Maze() {
        ObjectQuery<GameObject> loaded = Barrows.get().getContext().objects.select().id(Door.DOOR_IDS);
        doors = new Door[loaded.size()];

        Iterator<GameObject> iterator = loaded.iterator();
        for (int i = 0; i < doors.length; i++)
            doors[i] = new Door(iterator.next());
    }

    public Door[] pathToCenter() {
        GameObject chest;
        Iterator<GameObject> chestIter = Barrows.get().getContext().objects.select().id(CHEST_ID).iterator();

        if (!chestIter.hasNext())
            return null;
        chest = chestIter.next();

        Door[] doorsForChest = nearestsTo(chest);
        System.out.println(chest.getLocation());
        Door[] doorsForMe = nearestsTo(Barrows.get().getContext().players.getLocal());

        for (Door door : doorsForMe) {
            for (Door door2 : doorsForChest) {
                Door[] path = findPath(door, door2);

                if (path.length > 0)
                    return path;
            }
        }

        return null;
    }

    public Door[] findPath(Door from, final Door to) {
        Node path;

        if (!from.canOpen() || from.equals(to))
            return new Door[0];

        path = new Node(null, null, from);

        Door[] connections = from.getConnections();
        Set<Door> checked = new HashSet<>();
        PriorityQueue<Door> open = new PriorityQueue<>(doors.length, new Comparator<Door>() {
            @Override
            public int compare(Door o1, Door o2) {
                return costH(o1, to) - costH(o2, to);
            }
        });

        System.out.println("AAA");
        Collections.addAll(open, connections);

        Timer timeout = new Timer(1000 * 10);
        while (!open.isEmpty() && !to.equals(path.getMe()) && timeout.isRunning()) {
            System.out.println(to + ", " + path.getMe() + ", " + !to.equals(path.getMe()));
            Door poll = open.poll();

            if (!poll.canOpen())
                continue;

            connections = poll.getConnections();
            for (Door connection : connections)
                if (!checked.contains(connection))
                    open.add(connection);

            checked.add(poll);
            open.remove(poll);

            path = path.setChild(new Node(path, null, poll));

            poll.getDoor().click(true);
        }

        return path(path);
    }

    private Door[] nearestsTo(final Locatable origin) {
        ObjectQuery<GameObject> loaded = Barrows.get().getContext().objects.id(Door.DOOR_IDS);
        Door[] doors = new Door[loaded.size()];
        PriorityQueue<GameObject> objects = new PriorityQueue<>(loaded.size(), new Comparator<GameObject>() {
            @Override
            public int compare(GameObject o1, GameObject o2) {
                return (int) (o1.getLocation().distanceTo(origin) - o2.getLocation().distanceTo(origin));
            }
        });

        for (GameObject aLoaded : loaded)
            objects.add(aLoaded);

        Iterator<GameObject> iterator = objects.iterator();
        System.out.println(doors.length + ", " + objects.size());
        for (int i = 0; i < doors.length; i++) {
            doors[i] = new Door(iterator.next());
        }

        return doors;
    }

    private Door[] path(Node path) {
        Deque<Door> doors = new ArrayDeque<>();

        Node parent = path;

        do {
            doors.offerFirst(parent.getMe());
        } while ((parent = parent.getParent()) != null);

        return doors.toArray(new Door[doors.size()]);
    }

    private int costH(Door from, Door to) {
        GameObject fromDoor = from.getDoor();
        GameObject toDoor = to.getDoor();
        Tile fromTile;
        Tile toTile;

        if (fromDoor == null || toDoor == null)
            return Integer.MAX_VALUE;

        fromTile = fromDoor.getLocation();
        toTile = toDoor.getLocation();

        return Math.abs(toTile.getX() - fromTile.getX())
                + Math.abs(toTile.getY() - fromTile.getY());
    }
}
