package com.virus.barrows.api.combat;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.lang.ItemQuery;
import org.powerbot.script.util.Filter;
import org.powerbot.script.wrappers.Item;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 * User: Tommy
 * Date: 7/1/12
 * Time: 2:15 PM
 */
public enum Food {
    ANCHOVIES("Anchovies", 319, 10),
    CABBAGE("Cabbage", 1965, 10),
    CABBAGE2("Cabbage", 1967, 10),
    CRAYFISH("Crayfish", 13433, 10),
    EQUA_LEAVES("Equa leaves", 2128, 10),
    ONION("Onion", 1957, 10),
    POTATO("Potato", 1942, 1),
    BANANA("Banana", 1963, 20),
    CHEESE("Cheese", 1985, 20),
    DWELLBERRIES("Dwellberries", 2126, 20),
    JANGERBERRIES("Jangerberries", 247, 20),
    LIME("Lime", 2120, 20),
    LEMON("Lemon", 2102, 20),
    ORANGE("Orange", 2108, 20),
    SPICY_SAUCE("Spicy sauce", 7072, 20),
    SPICY_TOMATO("Spicy tomato", 9994, 20),
    SPINACH_ROLL("Spinach roll", 1969, 20),
    TOMATO("Tomato", 1982, 20),
    COOKED_MEAT("Cooked meat", 2142, 30),
    COOKED_CHICKEN("Cooked chicken", 2140, 30),
    CHOCOLATE_BAR("Chocolate bar", 1973, 30),
    KARAMBWANJI("Karambwanji", 3151, 30),
    SHRIMPS("Shrimps", 315, 30),
    UGHTHANKI_MEAT("Ughthanki meat", 1861, 30),
    BAKED_POTATO("Baked potato", 6701, 40),
    SARDINE("Sardine", 325, 40),
    BREAD("Bread", 2309, 50),
    CHILLI_CON_CARNE("Chilli con carne", 7062, 50),
    COOKED_RABBIT("Cooked rabbit", 3228, 50),
    HERRING("Herring", 347, 50),
    FRIED_MUSHROOMS("Fried mushrooms", 7082, 50),
    FRIED_ONIONS("Fried onions", 7084, 50),
    SCRAMBLED_EGG("Scrambled egg", 7078, 50),
    THIN_SNAIL_MEAT("Thin snail meat", 3369, 60),
    SKEWERED_KEBAB("Skewered kebab", 15123, 60),
    LEAN_SNAIL_MEAT("Lean snail meat", 3371, 70),
    MACKEREL("Mackerel", 355, 60),
    CHOCCHIP_CRUNCHIES("Chocchip crunchies", 2209, 70),
    CHOC_ICE("Choc-ice", 6794, 70),
    COD("Cod", 339, 70),
    FAT_SNAIL_MEAT("Fat snail meat", 3373, 70),
    SPICY_CRUNCHIES("Spicy crunchies", 9540, 70),
    TROUT("Trout", 333, 70),
    ROAST_BEAST_MEAT("Roast beast meat", 9988, 80),
    COOKED_SLIMY_EEL("Cooked slimy eel", 3381, 80),
    EGG_AND_TOMATO("Egg and tomato", 7064, 80),
    PAPAYA_FRUIT("Papaya fruit", 5972, 80),
    PEACH("Peach", 6883, 80),
    PIKE("Pike", 351, 80),
    TOAD_CRUNCHIES("Toad crunchies", 9538, 80),
    WORM_CRUNCHIES("Worm crunchies", 9542, 80),
    SPIDER_ON_STICK("Spider on stick", 6293, 90),
    SALMON("Salmon", 329, 90),
    JUG_OF_WINE("Jug of wine", 1993, 90),
    CAVE_EEL("Cave eel", 5003, 100),
    CAVE_EEL2("Cave eel", 5007, 100),
    COOKED_CHOMPY("Cooked chompy", 7228, 100),
    HALF_A_REDBERRY_PIE("Half a redberry pie", 2333, 5),
    REDBERRY_PIE("Redberry pie", 2325, 100),
    TUNA("Tuna", 361, 100),
    CHEESE_AND_TOMATO_BATTA("Cheese+tom batta", 2259, 110),
    COOKED_FISHCAKE("Cooked fishcake", 7530, 110),
    FRUIT_BATTA("Fruit batta", 9527, 110),
    LAVA_EEL("Lava eel", 2149, 110),
    HALF_A_MEAT_PIE("Half a meat pie", 2331, 50),
    MEAT_PIE("Meat pie", 2327, 100),
    STEW("Stew", 2003, 110),
    TOAD_BATTA("Toad batta", 2255, 101),
    VEGETABLE_BATTA("Vegetable batta", 2281, 110),
    WORM_BATTA("Worm batta", 2253, 110),
    SLICE_OF_CAKE("Slice of cake", 1895, 40),
    TWO_THIRD_CAKE("2/3 cake", 1893, 40),
    CAKE("Cake", 1891, 120),
    PART_FISH_PIE("Part fish pie", 7182, 40),
    PART_FISH_PIE2("Part fish pie", 7184, 40),
    HALF_A_FISH_PIE("Half a fish pie", 7190, 40),
    FISH_PIE("Fish pie", 7188, 80),
    PART_GARDEN_PIE("Part garden pie", 7172, 40),
    PART_GARDEN_PIE2("Part garden pie", 7174, 40),
    HALF_A_GARDEN_PIE("Half a garden pie", 7180, 40),
    GARDEN_PIE("Garden pie", 7178, 80),
    LOBSTER("Lobster", 379, 120),
    WORM_HOLE("Worm hole", 2191, 120),
    BASS("Bass", 365, 130),
    TUNA_AND_CORN("Tuna and corn", 7068, 130),
    HALF_AN_APPLE_PIE("Half an apple pie", 2335, 70),
    APPLE_PIE("Apple pie", 2323, 140),
    CHILLI_POTATO("Chilli potato", 7054, 140),
    COOKED_OOMLIE_WRAP("Cooked oomlie wrap", 2343, 140),
    HALF_PLAIN_PIZZA("1/2 plain pizza", 2291, 70),
    PLAIN_PIZZA("Plain pizza", 2289, 140),
    POTATO_WITH_BUTTER("Potato with butter", 6703, 140),
    SWORDFISH("Swordfish", 373, 140),
    CHOCOLATE_BOMB("Chocolate bomb", 2185, 150),
    CHOCOLATE_SLICE("Chocolate slice", 1901, 50),
    TWO_THIRD_CHOCOLATE_CAKE("2/3 chocolate cake", 1899, 50),
    CHOCOLATE_CAKE("Chocolate ake", 1897, 120),
    COOKED_JUBBLY("Cooked jubbly", 7568, 150),
    TANGLED_TOADS_LEGS("Tangled toads' legs", 9551, 160),
    EGG_POTATO("Egg potato", 7056, 160),
    HALF_MEAT_PIZZA("1/2 meat pizza", 2295, 80),
    MEAT_PIZZA("Meat pizza", 2293, 160),
    MONKFISH("Monkfish", 7946, 160),
    POTATO_WITH_CHEESE("Potato with cheese", 6705, 160),
    HALF_ANCHOVY_PIZZA("1/2 anchovy pizza", 2299, 90),
    ANCHOVY_PIZZA("Anchovy pizza", 2297, 180),
    ONE_THIRD_EVIL_TURNIP("1/3 evil turnip", 12138, 60),
    TWO_THIRDS_EVIL_TURNIP("2/3 evil turnip", 12136, 60),
    EVIL_TURNIP("Evil turnip", 12134, 180),
    KARAMBWAN("Karambwan", 3144, 180),
    CURRY("Curry", 2011, 190),
    UGHTHANKI_KEBAB("Ugthanki kebab", 1885, 190),
    MUSHROOM_POTATO("Mushroom potato", 7058, 200),
    SHARK("Shark", 385, 200),
    CAVEFISH("Cavefish", 15266, 200),
    SEA_TURTLE("Sea turtle", 397, 210),
    MANTA_RAY("Manta ray", 391, 220),
    HALF_PAPPLE_PIZZA("1/2 p'apple pizza", 2303, 110),
    PINEAPPLE_PIZZA("Pineapple pizza", 2301, 220),
    HALF_A_SUMMER_PIE("Half a summer pie", 7220, 110),
    PART_SUMMER_PIE("Part summer pie", 7212, 110),
    PART_SUMMER_PIE2("Part summer pie", 7214, 110),
    SUMMER_PIE("Summer pie", 7218, 220),
    TUNA_POTATO("Tuna potato", 7060, 220),
    HALF_A_WILD_PIE("Half a wild pie", 7210, 110),
    PART_WILD_PIE("Part wild pie", 7202, 110),
    PART_WILD_PIE2("Part wild pie", 7204, 110),
    WILD_PIE("Wild pie", 7208, 220),
    ROCKTAIL("Rocktail", 15272, 230),
    STRAWBERRY("Strawberry", 5504, 50),
    SWEETCORN("Sweetcorn", 5986, 50),
    WATERMELON_SLICE("Watermelon slice", 5984, 50),
    FROG_SPAWN("Frog spawn", 5004, 50),
    KEBAB("Kebab", 1971, 50);
    private String name;
    private int    id;
    private int    heal;

    private Food(String name, int id, int heal) {
        this.name = name;
        this.id = id;
        this.heal = heal;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getHeal() {
        return heal;
    }

    public boolean eat() {
        Item item = get();

        if (item != null) {
            item.interact("Eat");
            Sleep.waitForInventChange();
            return true;
        }
        return false;
    }

    public Item get() {
        Iterator<Item> iterator = Barrows.get().getContext().inventory.select().id(getId()).iterator();
        return iterator.hasNext() ? iterator.next() : null;
    }

    public ItemQuery<Item> getAll() {
        return Barrows.get().getContext().inventory.select().select(new Filter<Item>() {
            @Override
            public boolean accept(Item item) {
                return item.getId() == getId();
            }
        });
    }

    public int count() {
        ItemQuery<Item> item = getAll();
        int c = 0;

        for (Item cItem : item) {
            c += cItem.getStackSize();
        }
        return c;
    }

    public boolean contains() {
        return get() != null;
    }

    public static int[] toIDs(Food... food) {
        int[] arr = new int[food.length];
        for (int i = 0; i < arr.length; i++)
            arr[i] = food[i].getId();
        return arr;
    }

    public static Food[] getInventory() {
        List<Food> foods = new ArrayList<>();
        for (Item item : Barrows.get().getContext().inventory.getAllItems()) {
            for (Food food : Food.values()) {
                if (food.getId() == item.getId() && !foods.contains(food)) {
                    foods.add(food);
                }
            }
        }
        return foods.toArray(new Food[foods.size()]);
    }
}
