package com.virus.barrows.api.combat;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.methods.Prayer;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/12/13
 * Time: 1:37 PM
 */
public enum EnemyAttackStyle {
    MELEE {
        @Override
        protected Prayer.Effect defendCurses() {
            return Prayer.Curses.DEFLECT_MELEE;
        }

        @Override
        protected Prayer.Effect defendNormal() {
            return Prayer.Normal.PROTECT_FROM_MELEE;
        }
    },
    MAGIC {
        @Override
        protected Prayer.Effect defendCurses() {
            return Prayer.Curses.DEFLECT_MAGIC;
        }

        @Override
        protected Prayer.Effect defendNormal() {
            return Prayer.Normal.PROTECT_FROM_MAGIC;
        }
    },
    RANGE {
        @Override
        protected Prayer.Effect defendCurses() {
            return Prayer.Curses.DEFLECT_MISSILE;
        }

        @Override
        protected Prayer.Effect defendNormal() {
            return Prayer.Normal.PROTECT_FROM_MISSILES;
        }
    };

    public void defend() {
        MethodContext ctx = Barrows.get().getContext();

        System.out.println(ctx.prayer.getPoints());
        if (ctx.prayer.getPoints() > 0) {
            int book = ctx.settings.get(3277);
            Prayer.Effect effect;

            switch (book) {
                default:
                    return;

                case Prayer.PRAYER_BOOK_CURSES:
                    effect = defendCurses();
                    break;
                case Prayer.PRAYER_BOOK_NORMAL:
                    effect = defendNormal();
            }

            if (!ctx.prayer.isEffectActive(effect)) {
                ctx.prayer.setEffect(effect, true);
                waitForEffectChange(effect, true);
            }
        }
    }

    protected abstract Prayer.Effect defendCurses();

    protected abstract Prayer.Effect defendNormal();

    private static boolean waitForEffectChange(final Prayer.Effect effect, final boolean toChangeTo) {
        return Sleep.waitFor(new Condition() {
            @Override
            public boolean validate() {
                return Barrows.get().getContext().prayer.isEffectActive(effect) == toChangeTo;
            }
        }, 2000);
    }
}
