package com.virus.barrows.api.combat;

import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.game.Mouse;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.Game;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.methods.Settings;
import org.powerbot.script.methods.Widgets;
import org.powerbot.script.wrappers.Component;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/22/13
 * Time: 12:39 PM
 */
public class Magic {
    public static final int     ABILITY_TAB_SETTING = 682;
    public static final int     ABILITY_WIDGET      = 275;
    public static final int[][] SPELL_TAB_PATH      = {{40, 0x40000}, {45, 0x34}};
    public static final int     SPELLS_PARENT       = 16;


    public static void openTabs(MethodContext ctx) {
        ctx.game.openTab(Game.TAB_ABILITY_BOOK);
        final Settings settings = ctx.settings;
        Widgets widgets = ctx.widgets;

        for (int i = 0; i < SPELL_TAB_PATH.length; i++) {
            if ((settings.get(ABILITY_TAB_SETTING) & SPELL_TAB_PATH[i][1]) != SPELL_TAB_PATH[i][1]) {
                widgets.get(ABILITY_WIDGET, SPELL_TAB_PATH[i][0]).click(true);

                final int finalI = i;
                Sleep.waitFor(new Condition() {
                    @Override
                    public boolean validate() {
                        return (settings.get(ABILITY_TAB_SETTING) & SPELL_TAB_PATH[finalI][1]) == SPELL_TAB_PATH[finalI][1];
                    }
                }, 2000);
            }
        }
    }

    public static enum Spell {
        FIRE_WAVE,
        FIRE_BLAST,
        FIRE_BOLT,
        FIRE_STRIKE,
        BLOOD_BARRAGE,
        BLOOD_BLITZ,
        BLOOD_BURST,
        BLOOD_RUSH,
        WATER_SURGE,
        WATER_WAVE,
        WATER_BLAST,
        WATER_BOLT,
        WATER_STRIKE,
        ICE_BARRAGE,
        ICE_BLITZ,
        ICE_BURST,
        ICE_RUSH,
        EARTH_SURGE,
        EARTH_WAVE,
        EARTH_BLAST,
        EARTH_BOLT,
        EARTH_STRIKE,
        ROCK_BARRAGE,
        ROCK_BLITZ,
        ROCK_BURST,
        ROCK_RUSH,
        WIND_SURGE,
        WIND_WAVE,
        WIND_BLAST,
        WIND_BOLT,
        WIND_RUSH,
        GALE_BARRAGE,
        GALE_BLITZ,
        GALE_BURST,
        GALE_RUSH,
        FIRE_SURGE;
        public static final int AUTOCASTING_SETTING = 0;

        private int flag;
        private int componenent_texId;

        public static int getAutoCasting(MethodContext ctx) {
            return ctx.settings.get(AUTOCASTING_SETTING);
        }

        public void setTriggeredFlag(int flag) {
            this.flag = flag;
        }

        public boolean isAutocasting(MethodContext ctx) {
            return (ctx.settings.get(AUTOCASTING_SETTING) & flag) == flag;
        }

        private int getComponenenTextureId() {
            return componenent_texId;
        }

        private void setComponenenTextureId(int componenent_texId) {
            this.componenent_texId = componenent_texId;
        }

        public int autocast(final MethodContext ctx) {
            if (isAutocasting(ctx))
                return Mouse.POINT_NOT_ON_SCREEN;

            Component[] children = ctx.widgets.get(ABILITY_WIDGET, SPELLS_PARENT).getChildren();

            for (Component component : children)
                if (component.getTextureId() == getComponenenTextureId())
                    return Mouse.interact(component, "Auto-cast");

            return Mouse.POINT_NOT_ON_SCREEN;
        }
    }
}
