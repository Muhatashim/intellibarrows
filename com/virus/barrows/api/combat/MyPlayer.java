package com.virus.barrows.api.combat;

import com.virus.barrows.Barrows;
import com.virus.barrows.Config;
import org.powerbot.script.wrappers.Actor;
import org.powerbot.script.wrappers.Npc;
import org.powerbot.script.wrappers.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 4/9/13
 * Time: 11:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class MyPlayer {

    /**
     * @return if the interacting player is null or is not an instance of NPC
     */
    public static boolean isInCombat() {
        Player player = Barrows.get().getContext().players.getLocal();
        Actor interacting = player.getInteracting();
        return interacting != null && interacting instanceof Npc;
    }

    public static List<Npc> getInteracters() {
        List<Npc> interacters = new ArrayList<>();

        for (Npc npc : Barrows.get().getContext().npcs.select()) {
            Actor interacting = npc.getInteracting();
            if (interacting != null && interacting.equals(Barrows.get().getContext().players.getLocal()))
                interacters.add(npc);
        }

        return interacters;
    }

    public static boolean hasLowHealth() {
        return Barrows.get().getContext().players.getLocal()
                .getHealthPercent() <= (Config.get().useRandomizeMinHp() ? Config.get().getRandomHP() : Config.get().getMinHP());
    }

    public static boolean eat() {
        Food[] food = Config.get().getFood();
        for (Food cFood : food) {
            if (cFood.eat()) {
                Config.get().nextRandomHP();
                return true;
            }
        }

        return false;
    }

    public static int foodAmt() {
        int c = 0;
        Food[] food = Config.get().getFood();

        for (Food cFood : food)
            c += cFood.count();

        return c;
    }

    //RSBot's Inventory.contains method is f'd up.
    public static boolean has(int... ids) {
        for (int i : ids) {
            if (Barrows.get().getContext().inventory.select().id(i).size() > 0)
                return true;
        }
        return false;
    }
}
