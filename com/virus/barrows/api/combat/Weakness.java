package com.virus.barrows.api.combat;

import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.MethodContext;

import static com.virus.barrows.api.combat.Magic.Spell.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/21/13
 * Time: 12:59 PM
 */
public enum Weakness {
    FIRE(FIRE_SURGE,
            FIRE_WAVE,
            FIRE_BLAST,
            FIRE_BOLT,
            FIRE_STRIKE,

            BLOOD_BARRAGE,
            BLOOD_BLITZ,
            BLOOD_BURST,
            BLOOD_RUSH),

    WATER(WATER_SURGE,
            WATER_WAVE,
            WATER_BLAST,
            WATER_BOLT,
            WATER_STRIKE,

            ICE_BARRAGE,
            ICE_BLITZ,
            ICE_BURST,
            ICE_RUSH),

    EARTH(EARTH_SURGE,
            EARTH_WAVE,
            EARTH_BLAST,
            EARTH_BOLT,
            EARTH_STRIKE,

            ROCK_BARRAGE,
            ROCK_BLITZ,
            ROCK_BURST,
            ROCK_RUSH),

    AIR(WIND_SURGE,
            WIND_WAVE,
            WIND_BLAST,
            WIND_BOLT,
            WIND_RUSH,

            GALE_BARRAGE,
            GALE_BLITZ,
            GALE_BURST,
            GALE_RUSH);

    private final Magic.Spell[] spells;

    private Weakness(Magic.Spell... spells) {
        this.spells = spells;
    }

    public void useAgainstEnemy(final MethodContext ctx) {
        for (Magic.Spell spell : spells)
            spell.autocast(ctx);

        Sleep.waitFor(new Condition() {
            @Override
            public boolean validate() {
                for (Magic.Spell spell : spells)
                    if (!spell.isAutocasting(ctx))
                        return false;

                return true;
            }
        }, 2000);
    }
}
