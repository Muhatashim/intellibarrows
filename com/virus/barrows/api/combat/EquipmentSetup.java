package com.virus.barrows.api.combat;

import com.virus.barrows.Config;
import com.virus.barrows.api.WearableItem;
import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.Equipment;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/21/13
 * Time: 11:37 AM
 */
public class EquipmentSetup {
    private List<? extends WearableItem> items;

    public static EquipmentSetup newSetup(MethodContext ctx) {
        EquipmentSetup setup = new EquipmentSetup();
        List<WearableItem> items = new ArrayList<>();

        for (Equipment.Slot slot : Equipment.Slot.values()) {
            Item item = ctx.equipment.getItem(slot);

            if (item != null) {
                items.add(new WearableItem(true, item.getId(), slot));
            }
        }

        setup.setItems(items);
        return setup;
    }

    public static Setup getClosestMatchToPlayer(MethodContext ctx) {
        EquipmentSetup.Setup closest = null;
        int closestVal = -1;
        for (EquipmentSetup.Setup setup : EquipmentSetup.Setup.values()) {
            int cVal = setup.getSetup().getMatchToPlayer(ctx);

            if (cVal > closestVal) {
                closest = setup;
                closestVal = cVal;
            }
        }

        return closest;
    }

    public List<? extends WearableItem> getItems() {
        return items;
    }

    public void setItems(List<? extends WearableItem> items) {
        this.items = items;
    }

    public void setup(final MethodContext ctx) {
        for (WearableItem item : items)
            item.checkWear(ctx);

        for (final WearableItem item : items)
            Sleep.waitFor(new Condition() {
                @Override
                public boolean validate() {
                    return item.checkWear(ctx);
                }
            }, 2000);
    }

    public int getMatchToPlayer(MethodContext ctx) {
        int i = 0;

        for (WearableItem item : items)
            if (item.equipped(ctx))
                i++;

        return i;
    }

    public static enum Setup {
        MELEE,
        RANGE,
        MAGE;
        private EquipmentSetup setup;

        private Setup() {
            setSetup(
                    Config.get().getSetups()[ordinal()]);
        }

        public EquipmentSetup getSetup() {
            return setup;
        }

        public void setSetup(EquipmentSetup setup) {
            this.setup = setup;
        }
    }
}
