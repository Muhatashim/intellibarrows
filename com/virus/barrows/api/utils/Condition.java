package com.virus.barrows.api.utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 4/10/13
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Condition {
    public boolean validate();
}
