package com.virus.barrows.api.utils;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 4:40 PM
 */
public class Utils {

    /**
     * @param i  lower bound (inclusive)
     * @param i2 upper bound (exclusive)
     * @return a random int between i and i2.
     */
    public static int random(int i, int i2) {
        return ThreadLocalRandom.current().nextInt(i, i2);
    }

    /**
     * @param threshold 1 < threshold < 100
     * @return true if a random integer from 1 (inclusive)
     *         and 101 (exclusive) is below threshold.
     */
    public static boolean random(int threshold) {
        return random(1, 101) < threshold;
    }

    public static String formatNumber(Number num) {
        return NumberFormat.getNumberInstance(Locale.getDefault()).format(num);
    }

    public static void dumpStack() {
        Thread.dumpStack();
    }
//
//    public static Rectangle getGameScreenBounds() {
//        Area area = new Area(Widgets.get(548, 5).getBoundingRectangle());
//        WidgetChild abilityBar = EoC.getBar();
//        if (abilityBar.visible())
//            area.subtract(new Area(abilityBar.getBoundingRectangle()));
//        return area.getBounds();
//    }
//
//    public static boolean isOnScreen(Point p) {
//        return getGameScreenBounds().contains(p);
//    }
//
//    public static Future<?> threadedCameraTurn(final Locatable loc, final int diff) {
//        Runnable cameraTurn = new Runnable() {
//            @Override
//            public void run() {
//                Camera.turnTo(loc, diff);
//            }
//        };
//        return AIOFighter.get().submit(cameraTurn);
//    }
//
//    public static void onScreenInteract(org.powerbot.game.api.wrappers.interactive.Character entity, String action) {
//        onScreenInteract(entity, entity, action);
//    }
//
//    public static void onScreenInteract(Locatable locatable, Entity entity, String action) {
//        Future<?> future = null;
//        if (Utils.random(5))
//            future = Utils.threadedCameraTurn(locatable, 15);
//
//        Timer timout = new Timer(1000);
//        int interact = 0;
//        while (interact == 0 && timout.isRunning())
//            interact = Mouse.interact(entity, action);
//        if (interact == 1)
//            Sleep.waitFor(new Condition() {
//                @Override
//                public boolean validate() {
//                    return MyPlayer.isInCombat() || Sleep.waitForClick(1);
//                }
//            }, 3000);
//        else if (interact == -1)
//            Walking.walk(locatable); //TODO: Need custom walking using A*
//
//        if (future != null)
//            Sleep.waitForFuture(future);
//    }
}

