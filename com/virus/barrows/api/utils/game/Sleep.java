package com.virus.barrows.api.utils.game;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.Utils;
import org.powerbot.script.util.Random;
import org.powerbot.script.util.Timer;
import org.powerbot.script.wrappers.Tile;

import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 4/12/13
 * Time: 4:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class Sleep {
    public static void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static boolean waitForInventChange() {
        final int count = Barrows.get().getContext().inventory.count();
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return Barrows.get().getContext().inventory.count() != count;
            }
        }, 4000);
    }

    public static boolean waitFor(Condition cond, int maxWait) {
        Timer waitTimer = new Timer(Random.nextInt(Math.max(maxWait - 200,0), maxWait + 200));
        while (!cond.validate() && waitTimer.isRunning()) {
            sleep(100);
        }
        return waitTimer.isRunning();
    }

    public static boolean waitForFuture(final Future future) {
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return future.isDone();
            }
        }, 5000);
    }

    public static boolean waitForClick() {
        return waitForClick(2);
    }

    public static boolean waitForClick(final int button) {
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return Barrows.get().getContext().getClient().getCrossHairType() == button;
            }
        }, Utils.random(1000, 1200));
    }

    public static boolean waitForAnimationChange() {
        final int start = Barrows.get().getContext().players.getLocal().getAnimation();
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return Barrows.get().getContext().players.getLocal().getAnimation() != start;
            }
        }, Utils.random(1000, 1200));
    }

    public static boolean waitForAnimation() {
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return Barrows.get().getContext().players.getLocal().getAnimation() != -1;
            }
        }, Utils.random(1000, 1200));
    }

    public static boolean waitForAnimation(final int animation) {
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return Barrows.get().getContext().players.getLocal().getAnimation() == animation;
            }
        }, Utils.random(1000, 1200));
    }

    public static boolean waitForMovement() {
        final Tile start = Barrows.get().getContext().players.getLocal().getLocation();
        return waitFor(new Condition() {
            @Override
            public boolean validate() {
                return !start.equals(Barrows.get().getContext().players.getLocal().getLocation());
            }
        }, Utils.random(800, 3000));
    }

    /**
     * Waits the current thread until <tt>cond</tt> is <tt>true</tt>.
     * <p/>
     * This method will keep on checking <tt>cond</tt> every 10 ms until <tt>cond</tt> is
     * valid or until <tt>maxWait</tt> time has been passed.
     *
     * @param cond    The condition to be tested.
     * @param minWait Minimum time (ms) to wait before checking condition.
     * @param maxWait Maximum time (ms) to keep checking for.
     * @return <tt>cond.validate()</tt> after <tt>minWait</tt> time has been passed over.
     * @see Sleep#waitFor(Condition, int)
     * @see Sleep#waitForInventChange()
     */
    public static boolean waitFor(Condition cond, int minWait, int maxWait) {
        sleep(minWait);
        return waitFor(cond, maxWait);
    }
}
