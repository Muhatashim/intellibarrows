package com.virus.barrows.api.utils.game;

import com.virus.barrows.Barrows;
import org.powerbot.script.methods.Menu;
import org.powerbot.script.methods.Widgets;
import org.powerbot.script.wrappers.Component;
import org.powerbot.script.wrappers.Interactive;

import java.awt.*;


/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/12/13
 * Time: 9:39 AM
 */
public class Mouse {

    public static final byte CLICKED             = 1;
    public static final byte OPTION_NOT_FOUND    = 0;
    public static final byte POINT_NOT_ON_SCREEN = -1;


    /**
     * @return -1 if point not on screen,
     *         0 if <tt>action</tt> is not a valid action on <tt>entity</tt>, or
     *         1 clicked selected action.
     */
    public static synchronized byte interact(Interactive entity, String action) {
        Menu menu = Barrows.get().getContext().menu;
        org.powerbot.script.methods.Mouse mouse = Barrows.get().getContext().mouse;
        Point randomPointOnScreen;

        if (entity instanceof Component)
            randomPointOnScreen = entity.getNextPoint();
        else
            randomPointOnScreen = entity.getInteractPoint();

        if (randomPointOnScreen != null)
            mouse.move(randomPointOnScreen.x, randomPointOnScreen.y);
        else
            return POINT_NOT_ON_SCREEN;


        String[] popupMenuText = getPopupMenuText();
        if (popupMenuText.length >= 1 && popupMenuText[0].equals(action)) {
            mouse.click(true);
            return CLICKED;
        }

        String[] actions = menu.getItems();
        for (String s : actions) {
            if (s.contains(action)) {
                menu.click(action);
                return CLICKED;
            }
        }
        return OPTION_NOT_FOUND;
    }

    /**
     * @return Stripped HTML tags of the popup menu when hovering over an <tt>Entity</tt>.
     */
    public static String[] getPopupMenuText() {
        Widgets widgets = Barrows.get().getContext().widgets;

        Component child = widgets.get(548, 435).getChild(0);

        if (child == null)
            return new String[0];

        String text = child.getText();
        if (text == null)
            return null;
        return text.split("<[^>]*>");
    }
}

