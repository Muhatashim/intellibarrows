package com.virus.barrows.api.utils;

import com.virus.barrows.Barrows;
import com.virus.barrows.tasks.UnknownAreaFailSafe;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 3:41 PM
 */
public abstract class ConditionalTask implements Runnable {

    private static boolean stop;

    public abstract boolean activate();

    protected abstract int work();

    public final void run() {
        while (!stop && activate()) {
            if (this != Barrows.get().unknownAreaFailSafe)
                UnknownAreaFailSafe.TIME_IN.reset();

            int sleepTime = work();

            if (sleepTime == -1)
                break;

            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setStopFlag(boolean flag) {
        stop = flag;
    }
}
