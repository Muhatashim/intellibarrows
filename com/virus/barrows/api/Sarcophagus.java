package com.virus.barrows.api;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.chat.ChatOption;
import com.virus.barrows.api.chat.ChatOptions;
import com.virus.barrows.api.combat.MyPlayer;
import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.game.Mouse;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.lang.ObjectQuery;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.GameObject;
import org.powerbot.script.wrappers.Npc;
import org.powerbot.script.wrappers.Tile;

import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 6:37 PM
 */
public enum Sarcophagus {
    AHRIM(66017, new Tile(3567, 3288, 0)),
    DHAROK(63177, new Tile(3575, 3298, 0)),
    GUTHAN(66020, new Tile(3576, 3281, 0)),
    KARIL(66018, new Tile(3564, 3277, 0)),
    TORAG(66019, new Tile(3554, 3282, 0)),
    VERAC(66016, new Tile(3557, 3298, 0)),
    AKRISAE(61189, VERAC.getMoundLoc());

    private static final int[] SPADES     = {66115, 66116};
    private static final int[] STAIRCASES = {6702, 6703, 6704, 6705, 6706, 6707};
    private final int           sarcophagusId;
    private final Tile          moundLoc;
    private final MethodContext ctx;
    private       Ghost         forGhost;

    private Sarcophagus(int sarcophagusId, Tile moundLoc) {
        ctx = Barrows.get().getContext();

        this.sarcophagusId = sarcophagusId;
        this.moundLoc = moundLoc;
    }

    public Ghost getForGhost() {
        return forGhost != null ? forGhost : (forGhost = Ghost.valueOf(name()));
    }

    public Tile getMoundLoc() {
        return moundLoc;
    }

    public static GameObject getStairs() {
        Iterator<GameObject> iterator = Barrows.get().getContext().objects.select().id(STAIRCASES).nearest().iterator();
        return iterator.hasNext() ? iterator.next() : null;
    }

    public static boolean isInsideAMound() {
        return getStairs() != null;
    }

    /**
     * @return If the staircase is loaded on map.
     */
    public static boolean gtfo() {
        GameObject staircase = getStairs();

        if (staircase == null)
            return false;

        Mouse.interact(staircase, "Climb-up");
        return true;
    }

    public boolean insideMound() {
        Iterator<GameObject> iterator = ctx.objects.select().id(sarcophagusId).nearest().iterator();
        return iterator.hasNext() && iterator.next().getLocation().distanceTo(ctx.players.getLocal()) <= 8;
    }

    public boolean isAtMound() {
        return moundLoc.distanceTo(ctx.players.getLocal()) <= 5;
    }

    public void digIn() {
        if (!isAtMound())
            return;

        ObjectQuery<GameObject> spade = ctx.objects.select().id(SPADES).nearest();

        for (final GameObject obj : spade)
            Sleep.waitFor(new Condition() {
                @Override
                public boolean validate() {
                    return Mouse.interact(obj, "Dig-with") != Mouse.CLICKED;
                }
            }, 2000);
    }

    /**
     * @return -1 if the sarcophagus isn't loaded in the map yet;
     *         0 if misclicked;
     *         1 if the ghost is found;
     *         2 if the ghost leads to tunnel.
     */
    public int search() {
        Iterator<GameObject> sarcophaguses = ctx.objects.select().id(sarcophagusId).nearest().iterator();
        GameObject sarcophagus;
        final Ghost forGhost = getForGhost();

        if (!sarcophaguses.hasNext())
            return -1;
        sarcophagus = sarcophaguses.next();

        if (Mouse.interact(sarcophagus, "Search") == Mouse.CLICKED) {
            final int[] retVal = new int[1];

            Sleep.waitFor(new Condition() {
                @Override
                public boolean validate() {
                    ChatOption continueOption = ChatOptions.getContinueOption();
                    if (continueOption != null && continueOption.getComponent() != null) {
                        retVal[0] = 2;
                        return true;
                    }

                    List<Npc> interacter = MyPlayer.getInteracters();
                    for (Npc npc : interacter) {
                        System.out.println(npc + ", " + forGhost);
                        if (forGhost.isMe(npc.getId())) {
                            retVal[0] = 1;
                            return true;
                        }
                    }

                    return false;
                }
            }, 2000);

            return retVal[0];
        }

        return 0;
    }
}
