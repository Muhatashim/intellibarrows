package com.virus.barrows.api.transportations;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.Area;
import com.virus.barrows.api.WearableItem;
import com.virus.barrows.api.chat.ChatOptions;
import com.virus.barrows.api.utils.Condition;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.Equipment;
import org.powerbot.script.methods.MethodContext;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 3:35 PM
 */
public class DrakanTeleport extends Transportable {


    private final WearableItem  necklace;

    public DrakanTeleport(MethodContext ctx) {
        super(ctx);
        necklace = new WearableItem(
                false, 21576, Equipment.Slot.NECK
        );
    }

    @Override
    public boolean activate() {
        return necklace.checkWear(ctx)
                && !Area.BARROWS.insideArea();
    }

    @Override
    public int work() {
        System.out.println("1");
        necklace.interact("Teleport", ctx);
        System.out.println("2");

        if (Sleep.waitFor(new Condition() {
            @Override
            public boolean validate() {
                return ChatOptions.getOptions().size() > 0;
            }
        }, 2000))
            Barrows.get().getContext().keyboard.send("1", true);

        return -1;
    }
}
