package com.virus.barrows.api.transportations.emergencies;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.transportations.Transportable;
import org.powerbot.script.methods.MethodContext;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/13/13
 * Time: 10:04 AM
 */
public abstract class EmergencyTeleport extends Transportable {

    protected final int id;

    public EmergencyTeleport(int id, MethodContext ctx){
        super(ctx);
        this.id = id;
    }

    @Override
    public boolean activate() {
        return Barrows.get().getContext().inventory.contains(id);
    }
}
