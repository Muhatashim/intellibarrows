package com.virus.barrows.api.transportations.emergencies;

import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.Item;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/13/13
 * Time: 9:59 AM
 */
public class VarrockTele extends EmergencyTeleport {

    public VarrockTele(MethodContext ctx) {
        super(8007, ctx);
    }

    @Override
    protected int work() {
        Iterator<Item> iterator = ctx.inventory.id(id).iterator();
        if (!iterator.hasNext())
            return -1;

        Item item = iterator.next();

        item.interact("Break");
        return -1;
    }
}
