package com.virus.barrows.api.transportations;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.transportations.emergencies.VarrockTele;
import com.virus.barrows.api.utils.game.Sleep;
import org.powerbot.script.methods.MethodContext;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 3:28 PM
 */
public enum Transportation {

    DRAKANS_MADALLION("Drakan's Medallion", true, DrakanTeleport.class),
    VARROCK_TAB("Varrock Tele tab", true, VarrockTele.class);

    private final String  name;
    private final boolean toMainArea;

    private final Class<? extends Transportable> task;
    private       Transportable                  runnableInstance;

    private Transportation(String name, boolean toMainArea, Class<? extends Transportable> task) {
        this.name = name;
        this.toMainArea = toMainArea;
        this.task = task;
    }

    public boolean travel() {
        if (runnableInstance == null)
            try {
                Constructor<? extends Transportable> constructor = task.getConstructor(MethodContext.class);
                runnableInstance = constructor.newInstance(Barrows.get().getContext());
            } catch (InstantiationException | IllegalAccessException |
                    NoSuchMethodException | InvocationTargetException e) {
                e.printStackTrace();
                return false;
            }

        boolean activates = runnableInstance.activate();
        runnableInstance.run();

        Sleep.waitForMovement();

        return activates;
    }
}
