package com.virus.barrows.api.transportations;

import com.virus.barrows.api.utils.ConditionalTask;
import org.powerbot.script.methods.MethodContext;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/13/13
 * Time: 9:58 AM
 */
public abstract class Transportable extends ConditionalTask {
    protected final MethodContext ctx;

    public Transportable(MethodContext ctx) {
        this.ctx = ctx;
    }
}
