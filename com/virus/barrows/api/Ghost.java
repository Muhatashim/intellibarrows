package com.virus.barrows.api;

import com.virus.barrows.Barrows;
import com.virus.barrows.api.combat.EnemyAttackStyle;
import com.virus.barrows.api.combat.EquipmentSetup;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 6:35 PM
 */
public enum Ghost {
    VERAC(2030, 0x20, EnemyAttackStyle.MELEE, EquipmentSetup.Setup.MAGE),
    AKRISAE(new int[]{14297, 14295, 14298, 14299, 14300}, 0x40, EnemyAttackStyle.MAGIC, EquipmentSetup.Setup.RANGE),
    DHAROK(2026, 0x2, EnemyAttackStyle.MELEE, EquipmentSetup.Setup.MAGE),
    AHRIM(2025, 0x1, EnemyAttackStyle.MAGIC, EquipmentSetup.Setup.RANGE),
    TORAG(2029, 0x10, EnemyAttackStyle.MELEE, EquipmentSetup.Setup.MAGE),
    KARIL(2028, 0x8, EnemyAttackStyle.RANGE, EquipmentSetup.Setup.MELEE),
    GUTHAN(2027, 0x4, EnemyAttackStyle.MELEE, EquipmentSetup.Setup.MAGE);

    private final int[]                ids;
    private final int                  settingsFlag;
    private final EnemyAttackStyle     enemyAttackStyle;
    private final EquipmentSetup.Setup setup;
    private       boolean              use;
    private       Sarcophagus          sarcophagus;

    private Ghost(int[] ids, int settingsFlag, EnemyAttackStyle enemyAttackStyle, EquipmentSetup.Setup setup) {
        this.ids = ids;
        this.settingsFlag = settingsFlag;
        this.enemyAttackStyle = enemyAttackStyle;
        this.setup = setup;
        this.use = true;
    }

    private Ghost(int id, int settingsFlag, EnemyAttackStyle enemyAttackStyle, EquipmentSetup.Setup setup) {
        this(new int[]{id}, settingsFlag, enemyAttackStyle, setup);
    }

    public static int ghostsToKill() {
        int c = 0;

        for (Ghost ghost : values())
            if (ghost.use())
                c++;
        return c;
    }

    public EnemyAttackStyle getEnemyAttackStyle() {
        return enemyAttackStyle;
    }

    public boolean use() {
        return use;
    }

    public void setUse(boolean use) {
        this.use = use;
    }

    public boolean isDead() {
        return (Barrows.get().getContext().settings.get(1513) & settingsFlag) == settingsFlag;
    }

    public boolean isMe(int id) {
        for (int cId : ids)
            if (cId == id)
                return true;

        return false;
    }

    public int[] getIds() {
        return ids;
    }

    public Sarcophagus getSarcophagus() {
        return sarcophagus != null ? sarcophagus : (sarcophagus = Sarcophagus.valueOf(name()));
    }

    public EquipmentSetup.Setup getSetupType() {
        return setup;
    }
}
