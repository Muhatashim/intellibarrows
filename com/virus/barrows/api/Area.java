package com.virus.barrows.api;

import com.virus.barrows.Barrows;
import org.powerbot.script.wrappers.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/12/13
 * Time: 10:13 AM
 */
public enum Area {
    BARROWS(30, new Tile(3566, 3290, 0));

    private final int  radius;
    private final Tile center;

    private Area(int radius, Tile center) {
        this.radius = radius;
        this.center = center;
    }

    public int getRadius() {
        return radius;
    }

    public Tile getCenter() {
        return center;
    }

    public boolean insideArea() {
        return center.distanceTo(Barrows.get().getContext().players.getLocal()) <= radius;
    }
}
