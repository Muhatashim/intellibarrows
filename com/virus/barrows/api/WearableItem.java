package com.virus.barrows.api;

import com.virus.barrows.Barrows;
import org.powerbot.script.methods.Equipment;
import org.powerbot.script.methods.Game;
import org.powerbot.script.methods.MethodContext;
import org.powerbot.script.wrappers.Component;
import org.powerbot.script.wrappers.Item;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 3:49 PM
 */
public class WearableItem {

    private final boolean        forceWear;
    private final int            id;
    private final Equipment.Slot slot;

    public WearableItem(boolean forceWear, int id, Equipment.Slot slot) {
        this.forceWear = forceWear;
        this.id = id;
        this.slot = slot;
    }

    public Equipment.Slot getSlot() {
        return slot;
    }

    public int getId() {
        return id;
    }

    public static String getWearAction(Component itemComp) {
        String[] actions = itemComp.getActions();

        for (String s : actions) {
            if (s.equals("Wear"))
                return s;
            if (s.equals("Wield"))
                return s;
        }

        return null;
    }

    /**
     * Wears the item is <tt>forceWear</tt> is true.
     *
     * @return If the item is in the inventory or equipment.
     */
    public boolean checkWear(MethodContext ctx) {
        Iterator<Item> item = ctx.inventory.select().id(id).first().iterator();

        if (!isWearing(ctx) && forceWear && item.hasNext()) {
            Component comp = item.next().getComponent();
            comp.interact(getWearAction(comp));
        }

        return item == null || !forceWear && isWearing(ctx);
    }

    public Item getItem(MethodContext ctx) {
        if (isWearing(ctx)) {
            if (ctx.game.getCurrentTab() != Game.TAB_EQUIPMENT)
                ctx.game.openTab(Game.TAB_EQUIPMENT);
            return ctx.equipment.getItem(slot);
        } else {
            Iterator<Item> iterator = ctx.inventory.select().id(id).first().iterator();
            return iterator.hasNext() ? iterator.next() : null;
        }
    }

    public boolean interact(String interaction, MethodContext ctx) {
        Item item = getItem(ctx);

        if (item != null) {
            item.getComponent().interact(interaction);
            return true;
        }
        return false;
    }

    public boolean isWearing(MethodContext ctx) {
        Item item = ctx.equipment.getItem(slot);
        return item != null && item.getId() == id;
    }

    public boolean equipped(MethodContext ctx) {
        return Barrows.get().getContext().inventory.id(id).size() > 0
                || isWearing(ctx);
    }
}
