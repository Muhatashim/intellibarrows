package com.virus.barrows;

import com.virus.barrows.api.Ghost;
import com.virus.barrows.api.WearableItem;
import com.virus.barrows.api.combat.EquipmentSetup;
import com.virus.barrows.api.combat.Food;
import com.virus.barrows.api.maze.Door;
import com.virus.barrows.api.maze.Maze;
import com.virus.barrows.api.utils.ConditionalTask;
import com.virus.barrows.tasks.*;
import com.virus.barrows.ui.MainUI;
import javafx.application.Application;
import org.powerbot.event.PaintListener;
import org.powerbot.script.Manifest;
import org.powerbot.script.PollingScript;
import org.powerbot.script.methods.Equipment;
import org.powerbot.script.wrappers.GameObject;

import java.awt.*;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/11/13
 * Time: 3:15 PM
 */

@Manifest(
        name = "IntelliBarrows",
        description = "Kills the Barrows brothers intelligently.",
        authors = "VIRUS",
        version = 0.1
)
public class Barrows extends PollingScript implements PaintListener {

    private static Barrows instance;

    private final ExecutorService executor;

    public final ConditionalTask[] tasks;

    public final GhostAttacker       ghostAttacker;
    public final EquipmentSwitcher   equipmentSwitcher;
    public final SarcophagusFinder   sarcophagusFinder;
    public final UnknownAreaFailSafe unknownAreaFailSafe;

    public Barrows() {
        executor = Executors.newFixedThreadPool(4);

        tasks = new ConditionalTask[]{
                ghostAttacker = new GhostAttacker(ctx),
                equipmentSwitcher = new EquipmentSwitcher(ctx),
                sarcophagusFinder = new SarcophagusFinder(ctx),
                unknownAreaFailSafe = new UnknownAreaFailSafe(),
                new Banker(ctx),
                new MazeTraveler(ctx)
        };

        Config.get().setFood(new Food[]{Food.SHARK});
        Config.get().setMinHP(70);

        Config.get().setSetups(new EquipmentSetup[]{
                new EquipmentSetup() {{
                    setItems(Arrays.asList(
                            new WearableItem(true, 18349, Equipment.Slot.MAIN_HAND), //chaotic rapier
                            new WearableItem(true, 13736, Equipment.Slot.OFF_HAND))); //blessed sheild
                }},
                new EquipmentSetup() {{
                    setItems(Arrays.asList(
                            new WearableItem(true, 19830, Equipment.Slot.MAIN_HAND), //charge bow
                            new WearableItem(true, 13736, Equipment.Slot.OFF_HAND),//blessed sheild
                            new WearableItem(true, 892, Equipment.Slot.OFF_HAND))); //rune arrows
                }},
                new EquipmentSetup() {{
                    setItems(Arrays.asList(
                            new WearableItem(true, 15486, Equipment.Slot.MAIN_HAND))); //staff
                }}
        });

        Ghost.AKRISAE.setUse(false);

        instance = this;
    }

    @Override
    public void start() {
        Application.launch(MainUI.class);
    }

    @Override
    public int poll() {
        if (Config.get().isSetup())
            try {

                for (ConditionalTask node : tasks)
                    node.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
//        doors = maze.pathToCenter();

//        System.out.println(Arrays.toString(doors));

        return 200;
    }

    @Override
    public void stop() {
        ConditionalTask.setStopFlag(true);
        executor.shutdown();
    }

    public static Barrows get() {
        return instance == null ? instance = new Barrows() : instance;
    }

    public <T> Future<T> submit(Callable<T> task) {
        return executor.submit(task);
    }

    public <T> Future<T> submit(Runnable task, T result) {
        return executor.submit(task, result);
    }

    public Future<?> submit(Runnable task) {
        return executor.submit(task);
    }

    private Maze   maze  = null;
    private Door[] doors = null;

    @Override
    public void repaint(Graphics graphics) {
        graphics.setColor(Color.WHITE);

        int i = ctx.settings.get(1512);
//        System.out.println(i);

        for (GameObject obj : ctx.objects.select()) {
            if (Door.isDoor(obj.getId())) {
                Point p = obj.getLocation().getMatrix(ctx).getCenterPoint();

                if (obj.getModel() == null)
                    continue;


                int x = obj.getLocation().getX();
                int y = obj.getLocation().getY();

                graphics.drawString(obj.getId() + "", p.x, p.y += 10);
                graphics.drawString(((i | x) & y) + "", p.x, p.y += 10);
            }
        }


        if (doors == null)
            return;

        for (Door door : doors) {
            door.getLocation().getMatrix(ctx).draw(graphics);
        }
    }
}
